var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    email:{ 
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    salt: {
        type: String,
        required: true
    },
    birthdate: {
        type: Number,
        required: true
    },
    pictureURL: {
        type: String,
        default: 'profilePictures/default.jpg'
    },
    description: {
        type: String,
        default: ''
    },
    totalRating: {
        type: Number,
        default: 0
    },
    numRates: {
        type: Number,
        default: 0
    },
    categories: {
        type: [{type: Schema.Types.ObjectId, ref: 'Category'}]
    },
    radius: {
        type: Number,
        default: 0
    },
    eventsParticipating: {
        type: [{type: Schema.Types.ObjectId, ref: 'Event'}]
    },
    event: {
        type: Schema.Types.ObjectId, 
        ref: 'Event',
        default: null
    }
});

userSchema.statics.findNameAndBirthdate = function(uid, callback) {
    return this.findOne({_id: uid}, {
        'name': true,
        'birthdate': true
        // '_id': false,
        // '__v': false,
        // 'password': false,
        // 'salt': false
    }, callback);
};

userSchema.statics.findPersonalUser = function(uid, callback) {
    return this.findOne({_id: uid}, {
        '_id': true,
        'name': true,
        'description': true,
        'birthdate': true,
        'pictureURL': true,
        'rating': true,
        'categories': true,
        'radius': true,
        'eventsParticipating': true,
        'event': true
    }, callback);
};

module.exports = mongoose.model('User', userSchema);
