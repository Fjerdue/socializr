var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var chatMessageSchema = new Schema({
    timestamp: {
      type: Number,
      required: true
    },
    sender: {
      type: Schema.Types.ObjectId,
      ref: 'User',
      required: true
    },
    body: {
      type: String,
      required: true
    }
});

module.exports = mongoose.model('Chat', chatSchema);