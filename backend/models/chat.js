var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var chatSchema = new Schema({
    messages: {
        type: [{type: Schema.Types.ObjectId, ref: 'ChatMessage'}]
    }
});

module.exports = mongoose.model('Chat', chatSchema);