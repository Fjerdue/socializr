var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var eventSchema = new Schema({
    admin: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    // category: {
    //     type: Schema.Types.ObjectId,
    //     ref: 'Category',
    //     required: true
    // },
    category: {
        type: String,
        required: true
    },
    title: {
        type: String,
        required: true
    },
    maxParticipants: {
        type: Number,
        required: true
    },
    participants: {
        type: [{
            type: Schema.Types.ObjectId,
            ref: 'User'
        }]
    },
    description: {
        type: String
    },
    address: {
        type: String,
        required: true
    },
    timeCreated: {
        type: Number,
        required: true
    },
    timeStarting: {
        type: Number,
        required: true
    },
    deadline: {
        type: Number,
        required: true
    },
    queue: {
        type: [{
            type: Schema.Types.ObjectId,
            ref: 'User'
        }]
    },
    chat: {
        type: Schema.Types.ObjectId,
        ref: 'Chat'
    },
    pictureURL: {
        type: String,
        default: 'default.png'
    },
    minAge: {
        type: Number,
        required: true
    },
    maxAge: {
        type: Number,
        required: true
    }
});

eventSchema.statics.addUserToEventQueue = function(eventId, uid, callback) {
    this.findByIdAndUpdate(eventId, {
        $push: {
            queue: uid
        }
    }, {new: true}, callback);
};

eventSchema.statics.findByAdminId = function(adminId, callback) {
    this.findOne({admin: adminId}).populate('admin').exec(callback);
};

module.exports = mongoose.model('Event', eventSchema);