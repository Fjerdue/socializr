var crypto = require('crypto');

function security() {
    this.hash = function(data, salt, callback) {
        crypto.pbkdf2(data, salt, 125000, 64, 'sha512', function(err, key) {
            callback(err, key.toString('hex'));
        });
    };

    this.generateSalt = function() {
        return crypto.randomBytes(8).toString('hex');
    };
}

module.exports = new security();
