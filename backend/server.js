var Express = require('express');
var BodyParser = require('body-parser');
var mongoose = require('mongoose');
var jwt = require('jsonwebtoken');
var security = require('./security.js');
var fs = require('fs');

var UserModel = require('./models/user.js');
var CategoryModel = require('./models/category.js');
var EventModel = require('./models/event.js');

mongoose.Promise = global.Promise;

var options = {
	user: "userAdmin",
	pass: "bakkevej",
	auth: {
		authdb: "admin"
	}
};

mongoose.connect('mongodb://danielpc.dk/Socializr', options);

var secret = '1efba2f2b6337ec78142d252f5fc0472';

var app = Express();

app.use(BodyParser.json());
app.use(Express.static('public'))
app.disable('etag');

var router = Express.Router();
var tokenRouter = Express.Router();

tokenRouter.use(function(req, res, next) {
	var token = req.headers['x-access-token'];

	if(token) {
		jwt.verify(token, secret, function(err, decoded) {
			if(err) {
				res.sendStatus(401);
			} else {
				req.decoded = decoded;
				console.log(req.decoded);
				next();
			}
		});
	} else {
		res.sendStatus(401);
	}
});

app.use('/', router);
app.use('/', tokenRouter);

router.get('/test', function(req, res) {
	console.log('ikke secured');
	res.sendStatus(200);
});

router.post('/register', function(req, res) {
	var salt = security.generateSalt();
	
	security.hash(req.body.password, salt, function(hashErr, password) {
		if(hashErr) {
			console.log(hashErr);
			res.sendStatus(400);
			return;
		}
		
		var newUser = new UserModel({
			name: req.body.name,
			email: req.body.email,
			password: password,
			salt: salt,
			birthdate: req.body.birthdate
		});
		
		newUser.save(function(err) {
			if(err) {
				console.log(err);
				res.sendStatus(400);
			} else {
				res.sendStatus(200);
			}
		});
	});
});

tokenRouter.post('/changePassword', function(req, res) {
	UserModel.findById(req.decoded.uid, 'password salt', function(err, user) {
		if (err || req.body.newPass != req.body.confirmPass) {
			console.log(err);
			res.sendStatus(400);
			return;
		}

		security.hash(req.body.oldPass, user.salt, function(hashErr, oldHashedPwd) {
			if (user.password !== oldHashedPwd) {
				res.status(400).send("passwords don't match");
				return;
			} else if (hashErr) {
				console.log(hashErr);
				res.sendStatus(403);
				return;
			}

			security.hash(req.body.newPass, user.salt, function(hashErr, password) {
				if (hashErr) {
					console.log(hashErr);
					res.sendStatus(401);
					return;
				}

				UserModel.update({_id: req.decoded.uid}, {
					$set: {
						password: password
					}
				}, function(err, rawResponse) {
					if (err) {
						console.log(err);
						res.sendStatus(500);
						return;
					} else {
						console.log(rawResponse);
						res.sendStatus(200);
						return;
					}
				});
			});
		});
	});
});

tokenRouter.post('/updateDescription', function(req, res) {
	UserModel.update({_id: req.decoded.uid}, {
		$set: {
			description: req.body.desc
		}
	}, function(err, rawResponse) {
		if (err) {
			console.log(err);
			res.sendStatus(500);
			return;
		} else {
			res.sendStatus(200);
			return;
		}
	});
});

router.post('/login', function(req, res) {
	if(!req.body.email || !req.body.password) {
		console.log('Missing email or password');
		res.sendStatus(400);
		return;
	}

	UserModel.findOne({email: req.body.email}, function(err, user) {
		if(err || !user) {
			console.log(err);
			res.sendStatus(400);
			return;
		}

		security.hash(req.body.password, user.salt, function(err, hashedPassword) {
			if(err) {
				res.sendStatus(400);
				return;
			}

			if(user.password == hashedPassword) {
				var token = jwt.sign({'uid': user._id}, secret, {expiresIn: 60 * 120});
				console.log(token);
				res.send(token);
			} else {
				res.sendStatus(400);
			}
		});
	});
});

tokenRouter.get('/secured', function(req, res) {
	console.log('secured');
	res.sendStatus(200);
});

tokenRouter.get('/getUser', function(req, res) {
	UserModel.findNameAndBirthdate(req.decoded.uid, function(err, result) {
		if(err || !result) {
			res.sendStatus(400);
			return;
		}

		res.send(JSON.stringify(result));
	})
});

tokenRouter.get('/getPersonalUser', function(req, res) {
	UserModel.findPersonalUser(req.decoded.uid, function(err, result) {
		if (err || !result) {
			res.sendStatus(400);
			return;
		}

		console.log(req.decoded.uid);

		var response = JSON.stringify(result);
		console.log(response);
		res.send(response);
	});
});

tokenRouter.post('/createEvent', function(req, res) {
	var newEvent = new EventModel({
		admin: req.decoded.uid,
		category: req.body.eventCategory,
		title: req.body.eventTitle,
		maxParticipants: req.body.seats,
		address: req.body.eventAddress,
		timeCreated: Date.now(),
		timeStarting: req.body.eventDateTime,
		deadline: req.body.eventDeadlineDateTime,
		minAge: req.body.minAge,
		maxAge: req.body.maxAge
	});

	newEvent.save(function(err) {
		if (err) {
			console.log(err);
			res.sendStatus(400);
		} else {
			EventModel.findByAdminId(req.decoded.uid, function(err, result) {
				if(err || !result) {
					console.log('err: ' + err);
					console.log('result: ' + result);
					res.sendStatus(400);
				}

				console.log(result);
				res.status(200).send(result);
			});
		}
	})
});

tokenRouter.post('/uploadImage', function(req, res) {
	var decodedImage = Buffer.from(req.body.image, 'base64');
	var filename;
	var url;
	var eventid;

	if(req.body.folder === 'profilePictures') {			
		filename = req.decoded.uid + '.jpg';	
		url = req.body.folder + '/' + filename;
		finishImageUpload(res, decodedImage, req.body.folder, url, req.decoded.uid);
	} else if(req.body.folder === 'eventPictures') {
		EventModel.findOne({admin: req.decoded.uid}, function(err, event) {
			if(err || !event) {
				console.log('err: ' + err);
				console.log('event: ' + event);
				res.sendStatus(400);
				return;
			}

			filename = event._id + '.jpg';
			url = req.body.folder + '/' + filename;

			finishImageUpload(res, decodedImage, req.body.folder, url, event._id);
		});
	}	
});

function finishImageUpload(res, image, folder, url, id) {
	fs.writeFile('public/' + url, image, function(err) {
		if(err) {
			console.log('err: ' + err);
			res.sendStatus(500);
			return;
		}

		if(folder === 'profilePictures') {			
			UserModel.update({_id: id}, {
				$set: {
					pictureURL: url
				}
			}, function(err2, rawResponse) {
				if(err2) {
					console.log('err2: ' + err2);
					res.send(400);
					return;
				}

				res.sendStatus(200);
			});	
		} else if(folder === 'eventPictures') {
			EventModel.update({_id: id}, {
				$set: {
					pictureURL: url
				}
			}, function(err2, rawResponse) {
				if(err2) {
					console.log('err2: ' + err2);
					res.sendStatus(400);
					return;
				}
			});
			
			res.sendStatus(200);
		}
	});
}

tokenRouter.post('/addUserToEventQueue', function(req, res) {
	EventModel.addUserToEventQueue(req.body.eventId, req.decoded.uid, function(err, result) {
		if (err) {
			console.log(err);
			res.status(400).send("Something went wrong!");
			return;
		}

		res.status(200).send("user added to queue");
		return;
	});
});

tokenRouter.get('/getMyEvent', function(req, res) {
	EventModel.findByAdminId(req.decoded.uid, function(err, result) {
		if(err) {
			res.sendStatus(400);
			return;
		}
		
		if(result) {
			res.send(JSON.stringify(result));
		} else {
			res.sendStatus(404);
		}
	});
});

tokenRouter.post('/getEventsForCardStack', function(req, res) {
	var eventList = [], workers = [];

	for (var i = 0; i < req.body.noOfEvents; i++) {
		workers.push(new Worker(req.decoded.uid));
	}

	var callback = {
		exec: function(err, eventList) {
			if (err) {
				console.log(err);
				res.status(400).send("Didn't work");
				return;
			} else {
				res.status(200).send(JSON.stringify(eventList))
				return;
			}
		}
	};

	var waterfaller = new Waterfaller(workers, callback);

	waterfaller.runWorkers();
});

var Worker = function(uid, callback) {
	this.uid = uid;
	this.callback = callback;
	this.exec = function(err, eventList) {
		var self = this;
		findRandomEvent(uid, eventList, function(newErr, event) {
			if (event) {
				eventList.push(event);
			}
			
			self.callback.exec(newErr, eventList);
		});
	};
};

var Waterfaller = function(workers, callback) {
	this.workers = workers;
	this.callback = callback;
	this.runWorkers = function() {
		this.workers.push(callback);

		for (var i = 0; i < workers.length - 1; i++) {
			this.workers[i].callback = this.workers[i+1];
		}

		this.workers[0].exec(null, []);
	};
};

var findRandomEvent = function(uid, eventList, callback) {
	UserModel.findById(uid, {
		_id: true,
		eventsSeen: true,
		categories: true,
		birthdate: true,
		radius: true
	}, function(err, user) {
		if (err || !user) {
			callback(err);
			return;
		}

		var eventsToAvoid = user.eventsSeen.concat(eventList);

		var conditions = {
			_id: { $nin: eventsToAvoid },
			admin: { $ne: user._id },
			category: { $in: ["Chill"] }
		};

		EventModel.findRandomEvent(conditions, function(err, event) {
			callback(err, event);
		});
	});
};

app.listen(3000);

console.log('Server started on port 3000.');