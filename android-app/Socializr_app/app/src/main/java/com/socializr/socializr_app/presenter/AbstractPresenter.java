package com.socializr.socializr_app.presenter;

import com.socializr.socializr_app.networking.RestClient;

public abstract class AbstractPresenter {
    protected RestClient restClient;

    protected AbstractPresenter() {
        this.restClient = RestClient.getInstance();
    }
}
