package com.socializr.socializr_app.presenter;

import android.util.Log;

import com.socializr.socializr_app.application.Session;
import com.socializr.socializr_app.models.Event;
import com.socializr.socializr_app.networking.CreateEventMarshall;
import com.socializr.socializr_app.networking.PersonalEventDTO;
import com.socializr.socializr_app.networking.marshall.UploadImageMarshall;
import com.socializr.socializr_app.view.MyEvents.CreateEventActivity;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class CreateEventPresenter extends AbstractPresenter {
    private static final String TAG = "CreateEventPresenter";

    private CreateEventActivity activity;

    public CreateEventPresenter(CreateEventActivity activity) {
        this.activity = activity;
    }

    public void createEvent(CreateEventMarshall marshall, final UploadImageMarshall image) {
        Log.d(TAG, "createEvent called");

        restClient.createEvent(marshall, new Callback<Event>() {
            @Override
            public void onResponse(Response<Event> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    Log.d(TAG, "createEvent succeeded with response " + response.code());

                    Session.getInstance().setMyEvent(response.body());

                    restClient.uploadImage(image, new Callback<String>() {
                        @Override
                        public void onResponse(Response<String> res, Retrofit retrofit) {
                            if(res.isSuccess()) {
                                Log.d(TAG, "well...: " + res.code());
                                activity.onCreateEventSuccess();
                            } else {
                                activity.onPictureUploadFailed(res);
                            }
                        }

                        @Override
                        public void onFailure(Throwable t) {
                            activity.onLoginError(t);
                        }
                    });
                } else {
                    Log.d(TAG, "createEvent failed with response " + response.code());
                    activity.onCreateEventFailed(response);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.d(TAG, "login error: " + t.getMessage());
                System.err.println(t);

                activity.onLoginError(t);
            }
        });
    }
}
