package com.socializr.socializr_app.view.MyEvents;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.socializr.socializr_app.R;
import com.socializr.socializr_app.view.Profile.ProfileStrangerActivity;

public class AdminChatActivity extends Activity {

    private Button acceptButton;
    private ImageView eventImage;
    private LinearLayout llGuest;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_chat);

        acceptButton = (Button) findViewById(R.id.admin_chat_accept_button);
        eventImage = (ImageView) findViewById(R.id.admin_chat_event_image);
        llGuest = (LinearLayout) findViewById(R.id.llGuests);

        Intent intent = getIntent();

        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), AdminAcceptActivity.class));
            }
        });

        eventImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), EventInformationAcceptedActivity.class));
            }
        });

        llGuest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), ProfileStrangerActivity.class));
            }
        });
    }


}
