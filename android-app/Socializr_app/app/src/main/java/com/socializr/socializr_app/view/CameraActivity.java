package com.socializr.socializr_app.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.provider.MediaStore;

public abstract class CameraActivity extends Activity {
    private static final String[] OPTIONS = new String[] {
            "From camera",
            "From gallery"
    };

    private static final Intent[] ACTIONS = new Intent[] {
            new Intent(MediaStore.ACTION_IMAGE_CAPTURE),
            new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
    };

    public void choosePicture() {
        AlertDialog.Builder adBuilder = new AlertDialog.Builder(this);
        adBuilder.setItems(OPTIONS, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivityForResult(ACTIONS[which], which);
            }
        });

        adBuilder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK) {
            Bitmap image = null;

            if(requestCode == 0) {
                image = (Bitmap)data.getExtras().get("data");
            } else if(requestCode == 1) {
                try {
                    image = MediaStore.Images.Media.getBitmap(this.getContentResolver(), data.getData());
                } catch(Exception e) {
                    e.printStackTrace();
                }
            }

            onPicture(image);
        }
    }

    protected abstract void onPicture(Bitmap bitmap);
}
