package com.socializr.socializr_app.view;

import android.graphics.Bitmap;
import android.graphics.Camera;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.socializr.socializr_app.R;
import com.socializr.socializr_app.custom_view.SquareImageView;
import com.socializr.socializr_app.presenter.RegisterPresenter;
import com.socializr.socializr_app.util.AgeCalculator;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegisterActivity extends CameraActivity {
    private RegisterPresenter presenter;
    private Button cancel, create;
    private EditText birthdate, name, email, password1, password2;
    private FloatingActionButton registerFAB;
    private SquareImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        presenter = new RegisterPresenter(this);

        registerFAB = (FloatingActionButton)findViewById( R.id.registerUser_FAB );
        image = (SquareImageView) findViewById(R.id.registerImage_SquareImageView);
        name = (EditText)findViewById(R.id.name_editText);
        email = (EditText)findViewById(R.id.email_editText);
        password1 = (EditText)findViewById(R.id.password1_editText);
        password2 = (EditText)findViewById(R.id.password2_editText);

        birthdate = (EditText)findViewById(R.id.birthdate_editText);
        cancel = (Button)findViewById(R.id.cancel_button);
        create = (Button)findViewById(R.id.create_button);

        birthdate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() != MotionEvent.ACTION_DOWN)
                    return false;

                DatePickerFragment frag = new DatePickerFragment();

                frag.setCallback(new DatePickerFragment.OnSubmitListener() {
                    @Override
                    public void onSubmitted(int day, int month, int year) {
                        birthdate.setText(day + "/" + month + "/" + year);
                    }
                });

                frag.show(getFragmentManager(), "datePicker");
                return true;
            }
        });

        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createUser();
            }
        });

        password2.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == android.view.KeyEvent.ACTION_UP && keyCode == android.view.KeyEvent.KEYCODE_ENTER) {
                    createUser();
                    return true;
                }

                return false;
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        registerFAB.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                choosePicture();
            }
        } );
    }

    private void createUser() {
        String p1 = password1.getText().toString();
        String p2 = password2.getText().toString();
        String em = email.getText().toString();
        String n = name.getText().toString();
        Date d = null;
        Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);


        boolean valid = true;

        if(p1.isEmpty()) {
            password1.setError("Password must be filled");
            valid = false;
        }

        if(p2.isEmpty()) {
            password2.setError("Password must be repeated");
            valid = false;
        }

        if(!p1.equals(p2)) {
            password1.setError("Passwords don't match");
            password1.setError("Passwords don't match");
            valid = false;
        }

        if(em.isEmpty()) {
            email.setError("Email must be filled");
            valid = false;
        }

        if(!android.util.Patterns.EMAIL_ADDRESS.matcher(em).matches()){
            email.setError("Email must be valid");
            valid = false;
        }

        if(n.isEmpty()) {
            name.setError("Name must be filled");
            valid = false;
        }

        if(birthdate.getText().toString().isEmpty()) {
            Toast.makeText(this, "Birthdate must be set", Toast.LENGTH_LONG).show();
            valid = false;
        } else {
            DateFormat format = new SimpleDateFormat("d/M/y");
            try {
                d = format.parse(birthdate.getText().toString());

                if(AgeCalculator.getAgeFromDate(d) < 13) {
                    Toast.makeText(this, "You must be at least 13 years of age to use Socializr", Toast.LENGTH_LONG).show();
                    valid = false;
                }

            } catch(Exception e) {
                e.printStackTrace();
            }
        }

        if(valid) {
            presenter.register(n, em, p1, d);
            Toast.makeText( this, "You are now registered", Toast.LENGTH_SHORT ).show();
        }
    }

    public void onRegisterSuccess() {
        finish();
    }

    @Override
    protected void onPicture(Bitmap bitmap) {
        image.setImageBitmap(bitmap);
    }
}
