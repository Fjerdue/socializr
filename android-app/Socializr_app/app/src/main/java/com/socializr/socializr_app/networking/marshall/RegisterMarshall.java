package com.socializr.socializr_app.networking.marshall;

public class RegisterMarshall {
    private String name, email, password;
    private long birthdate;

    public RegisterMarshall(String name, String email, String password, long birthdate) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.birthdate = birthdate;
    }
}