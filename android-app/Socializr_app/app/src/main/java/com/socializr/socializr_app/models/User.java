package com.socializr.socializr_app.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class User {
    private String _id,
            name,
            email,
            description,
            pictureURL;

    private Long birthdate,
            radius,
            numRates,
            totalRating;

    @SerializedName("event")
    private Event eventAdministering;

    private List<String> categories;
    private List<Event> eventsParticipating, eventsSeen;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPictureURL() {
        return pictureURL;
    }

    public void setPictureURL(String pictureURL) {
        this.pictureURL = pictureURL;
    }

    public Long getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Long birthdate) {
        this.birthdate = birthdate;
    }

    public Long getRadius() {
        return radius;
    }

    public void setRadius(Long radius) {
        this.radius = radius;
    }

    public Long getNumRates() {
        return numRates;
    }

    public void setNumRates(Long numRates) {
        this.numRates = numRates;
    }

    public Long getTotalRating() {
        return totalRating;
    }

    public void setTotalRating(Long totalRating) {
        this.totalRating = totalRating;
    }

    public Event getEventAdministering() {
        return eventAdministering;
    }

    public void setEventAdministering(Event eventAdministering) {
        this.eventAdministering = eventAdministering;
    }

    public List<String> getCategories() {
        return categories;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    public List<Event> getEventsParticipating() {
        return eventsParticipating;
    }

    public void setEventsParticipating(List<Event> eventsParticipating) {
        this.eventsParticipating = eventsParticipating;
    }

    public List<Event> getEventsSeen() {
        return eventsSeen;
    }

    public void setEventsSeen(List<Event> eventsSeen) {
        this.eventsSeen = eventsSeen;
    }

    @Override
    public String toString() {
        return "User{" +
                "_id='" + _id + '\'' +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", description='" + description + '\'' +
                ", pictureURL='" + pictureURL + '\'' +
                ", birthdate=" + birthdate +
                ", radius=" + radius +
                ", numRates=" + numRates +
                ", totalRating=" + totalRating +
                ", eventAdministering=" + eventAdministering +
                ", categories=" + categories +
                ", eventsParticipating=" + eventsParticipating +
                ", eventsSeen=" + eventsSeen +
                '}';
    }
}
