package com.socializr.socializr_app.view.Settings;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.design.widget.FloatingActionButton;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.socializr.socializr_app.R;
import com.socializr.socializr_app.models.PersonalUserDTO;
import com.socializr.socializr_app.networking.marshall.UploadImageMarshall;
import com.socializr.socializr_app.presenter.EditProfilePresenter;
import com.socializr.socializr_app.application.Session;
import com.socializr.socializr_app.util.ImageEncoder;
import com.socializr.socializr_app.view.CameraActivity;
import com.socializr.socializr_app.view.Fragments.ChangePasswordFragment;

import java.io.ByteArrayOutputStream;

public class EditProfileActivity extends CameraActivity {

    private Button changePasswordButton;
    private ImageView profilePictureImageView;
    private EditProfilePresenter presenter;
    private Button cancelButton;
    private Button updateButton;
    private FloatingActionButton editProfileFAB;
    private TextView name;
    private EditText description;

    private boolean shouldUpdateProfilePicture;
    private Bitmap selectedPicture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        presenter = new EditProfilePresenter(this);

        PersonalUserDTO user = Session.getInstance().getUser();

        editProfileFAB = (FloatingActionButton) findViewById(R.id.editProfile_FAB);
        changePasswordButton = (Button) findViewById(R.id.changePassword_button);
        profilePictureImageView = (ImageView) findViewById(R.id.profilePicture_imageview);
        cancelButton = (Button) findViewById(R.id.editProfile_cancel_button);
        updateButton = (Button) findViewById(R.id.editProfile_update_button);
        name = (TextView)findViewById(R.id.name_textview);
        description = (EditText)findViewById(R.id.description_edittext);

        name.setText(user.getName() + ", " + user.getAge());
        description.setText(user.getDescription());

        selectedPicture = Session.getInstance().getProfilePicture();

        if(selectedPicture != null) {
            profilePictureImageView.setImageBitmap(selectedPicture) ;
        }

        changePasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChangePasswordFragment frag = new ChangePasswordFragment();
                frag.setCallback(new ChangePasswordFragment.OnSubmitListener() {
                    @Override
                    public void onSubmitted(String oldPass, String newPass, String confirmPass) {
                        presenter.changePassword(oldPass, newPass, confirmPass);
                    }
                });

                frag.show(getFragmentManager(), "changePassword");
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.updateDescription(description.getText().toString());

                if(shouldUpdateProfilePicture) {
                    shouldUpdateProfilePicture = false;
                    presenter.uploadProfilePicture(selectedPicture);
                    Session.getInstance().setProfilePicture(selectedPicture);
                }
            }
        });

        editProfileFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                choosePicture();
            }
        });
    }

    public void toast(String message) {
        Toast toast = Toast.makeText(this, message, Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    protected void onPicture(Bitmap bitmap) {
        profilePictureImageView.setImageBitmap(bitmap);
        shouldUpdateProfilePicture = true;
        selectedPicture = bitmap;
    }

    public void onDescriptionUpdated() {
        finish();
    }
}
