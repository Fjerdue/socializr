package com.socializr.socializr_app.util;

import java.util.Calendar;
import java.util.Date;

public class AgeCalculator {
    private AgeCalculator() {}

    public static int getAgeFromDate(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        Calendar today = Calendar.getInstance();

        int age = today.get(Calendar.YEAR) - cal.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < cal.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }

        return age;
    }
}
