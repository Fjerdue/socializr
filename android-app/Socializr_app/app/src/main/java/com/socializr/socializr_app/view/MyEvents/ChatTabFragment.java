package com.socializr.socializr_app.view.MyEvents;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.socializr.socializr_app.R;
import com.socializr.socializr_app.application.Session;
import com.socializr.socializr_app.models.Event;
import com.socializr.socializr_app.presenter.MainPresenter;
import com.socializr.socializr_app.view.Adapters.EventListAdapter;

import java.io.InputStream;
import java.net.URL;

public class ChatTabFragment extends Fragment {

    private EventListAdapter eventListAdapter;
    private ListView llTest;
    private ImageView myEventImage;
    private FloatingActionButton myeventFab;
    private TextView eventTitle;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_events, container, false);

        llTest = (ListView) view.findViewById(R.id.llTestList);
        myEventImage = (ImageView)view.findViewById(R.id.myEvent_imageview);
        myeventFab = (FloatingActionButton)view.findViewById( R.id.myevent_fab );
        eventTitle = (TextView)view.findViewById( R.id.myEventTitle_textView );

        eventTitle.setText( "" ); //make it empty if no event is active


        eventListAdapter = new EventListAdapter(getActivity());

        llTest.setAdapter(eventListAdapter);



        //FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);

        myeventFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent;

                if(Session.getInstance().getMyEvent() == null) {
                   intent = new Intent(getContext(), CreateEventActivity.class);
                } else {
                   intent = new Intent(getContext(), AdminChatActivity.class);
                }

                getContext().startActivity(intent);
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        final Event event = Session.getInstance().getMyEvent();
        Bitmap picture = Session.getInstance().getMyEventPicture();

        if(picture != null) {
            myEventImage.setImageBitmap(picture);
            myeventFab.setImageResource( R.mipmap.ic_create_white_48dp );
            eventTitle.setText(Session.getInstance().getMyEvent().getTitle());
        } else if(event != null) {
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void[] params) {
                    try {
                        Bitmap bitmap = BitmapFactory.decodeStream((InputStream)new URL("http://danielpc.dk:3000/" + event.getPictureURL()).getContent());
                        Session.getInstance().setMyEventPicture(bitmap);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    return null;
                }

                @Override
                protected void onPostExecute(Void o) {
                    super.onPostExecute(o);
                    myEventImage.setImageBitmap(Session.getInstance().getMyEventPicture());
                    myeventFab.setImageResource( R.mipmap.ic_create_white_48dp );
                    eventTitle.setText(Session.getInstance().getMyEvent().getTitle());
                }
            }.execute();
        }
    }
}
