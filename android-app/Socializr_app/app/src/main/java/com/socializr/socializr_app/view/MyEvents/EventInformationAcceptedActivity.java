package com.socializr.socializr_app.view.MyEvents;

import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.socializr.socializr_app.R;
import com.socializr.socializr_app.application.Session;

public class EventInformationAcceptedActivity extends AppCompatActivity {
    private ImageView eventPicture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_information_accepted);

        eventPicture = (ImageView)findViewById(R.id.eventPicture_image);
        eventPicture.setImageBitmap(Session.getInstance().getCurrentChatPicture());
    }
}
