package com.socializr.socializr_app.models;

public class UserTestDTO {
    private String name, birthdate;

    public UserTestDTO(String name, String birthdate) {
        this.name = name;
        this.birthdate = birthdate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    @Override
    public String toString() {
        return "UserTestDTO{" +
                "name='" + name + '\'' +
                ", birthdate='" + birthdate + '\'' +
                '}';
    }
}
