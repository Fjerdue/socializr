package com.socializr.socializr_app.view;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.socializr.socializr_app.R;
import com.socializr.socializr_app.application.Session;
import com.socializr.socializr_app.presenter.LoginPresenter;
import com.socializr.socializr_app.presenter.MainPresenter;

public class LoginActivity extends AppCompatActivity {

    private EditText email, password;
    private Button submit, cancel;
    private LoginPresenter loginPresenter;

    private LoginActivity that = this;

    private SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        this.loginPresenter = new LoginPresenter(this);

        this.email = (EditText) findViewById(R.id.email_text);
        this.password = (EditText) findViewById(R.id.password_text);
        this.submit = (Button) findViewById(R.id.login_button);
        this.cancel = (Button) findViewById(R.id.cancel_button);

        prefs = PreferenceManager.getDefaultSharedPreferences(this);

        String email = prefs.getString("email", null);

        if(email != null) {
            this.email.setText(email);
        }

        this.email.setText("hanne@hotmail.com"); //Nemt login til censor
        this.password.setText("Hanne1964"); //Nemt login til censor

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(that, IndexActivity.class));
                finish();
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });

        password.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_ENTER) {
                    login();
                    return true;
                }

                return false;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Session.getInstance().reset();
    }

    public void onLoginSuccess() {
        prefs.edit().putString("email", email.getText().toString()).apply();

        startActivity(new Intent(that, MainActivity.class));
        finish();
    }

    public void onLoginFailed(int statusCode) {
        String errMsg = null;

        switch (statusCode) {
            case 400:
                errMsg = "Wrong username and/or password";
                break;
            case 500:
                errMsg = "Server not found";
                break;
            default:
                errMsg = "Some error has occurred";
                break;
        }

        password.setError(errMsg);
    }

    private void login() {
        String emailText = email.getText().toString();
        String passwordText = password.getText().toString();

        loginPresenter.login(emailText, passwordText);
    }
}
