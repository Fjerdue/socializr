package com.socializr.socializr_app.view;

import android.content.Intent;
import android.graphics.Bitmap;
import android.media.Image;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.socializr.socializr_app.R;
import com.socializr.socializr_app.models.PersonalUserDTO;
import com.socializr.socializr_app.application.Session;
import com.socializr.socializr_app.view.Settings.EditFiltersActivity;
import com.socializr.socializr_app.view.Settings.EditProfileActivity;


public class SettingTabFragment extends Fragment {
    private Button logout, editProfile, editFilters;
    private TextView name, description;
    private ImageView profilePicture;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_setting_tab, container, false);

        PersonalUserDTO user = Session.getInstance().getUser();

        logout = (Button) view.findViewById(R.id.logout_button);
        editProfile = (Button) view.findViewById(R.id.edit_profile_button);
        editFilters = (Button) view.findViewById(R.id.edit_filters_button);
        description = (TextView) view.findViewById(R.id.description_textView);
        name = (TextView) view.findViewById(R.id.name_textView);
        profilePicture = (ImageView)view.findViewById(R.id.profilePicture_imageview);

        name.setText(user.getName() + ", " + user.getAge());
        description.setText(user.getDescription());

        Bitmap tmp = Session.getInstance().getProfilePicture();

        if(tmp != null) {
            profilePicture.setImageBitmap(tmp);
        }

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        editFilters.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), EditFiltersActivity.class));
            }
        });

        editProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), EditProfileActivity.class));
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        description.setText(Session.getInstance().getUser().getDescription());

        Bitmap tmp = Session.getInstance().getProfilePicture();

        if(tmp != null) {
            profilePicture.setImageBitmap(tmp);
        }
    }
}
