package com.socializr.socializr_app.models;

public class ChatDTO {

    private MessageDTO[] message;

    public ChatDTO(MessageDTO[] message) {
        this.message = message;
    }

    public MessageDTO[] getMessage() {
        return message;
    }

    public void setMessage(MessageDTO[] message) {
        this.message = message;
    }
}
