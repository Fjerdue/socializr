package com.socializr.socializr_app.view;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;

import com.socializr.socializr_app.R;

public class DatePickerFragment extends DialogFragment {
    public interface OnSubmitListener {
        void onSubmitted(int day, int month, int year);
    }

    private OnSubmitListener callback;

    private DatePicker datePicker;
    private Button okButton, cancelButton;

    public DatePickerFragment() {
        callback = null;
    }

    public void setCallback(OnSubmitListener callback) {
        this.callback = callback;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_datepicker, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        View view = getView();
        datePicker = (DatePicker)view.findViewById(R.id.datepicker);

        okButton = (Button)view.findViewById(R.id.datepicker_ok_button);
        cancelButton = (Button)view.findViewById(R.id.datepicker_cancel_button);

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onSubmitted(datePicker.getDayOfMonth(), datePicker.getMonth() + 1, datePicker.getYear());
                dismiss();
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }
}
