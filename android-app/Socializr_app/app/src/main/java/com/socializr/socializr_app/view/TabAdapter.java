package com.socializr.socializr_app.view;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.socializr.socializr_app.view.EventStack.EventTabFragment;
import com.socializr.socializr_app.view.MyEvents.ChatTabFragment;

public class TabAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public TabAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                SettingTabFragment tab1 = new SettingTabFragment();
                return tab1;
            case 1:
                EventTabFragment tab2 = new EventTabFragment();
                return tab2;
            case 2:
                ChatTabFragment tab3 = new ChatTabFragment();
                return tab3;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}