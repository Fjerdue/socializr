package com.socializr.socializr_app.networking.marshall;

public class UploadImageMarshall {
    public enum Type {
        PROFILE_PICTURE("profilePictures"),
        EVENT_PICTURE("eventPictures");

        private final String value;

        Type(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return value;
        }
    }

    private String image;
    private String folder;

    public UploadImageMarshall(String encodedImage, Type type) {
        image = encodedImage;
        folder = type.toString();
    }
}
