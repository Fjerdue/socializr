package com.socializr.socializr_app.presenter;

import android.graphics.Bitmap;
import android.util.Log;

import com.socializr.socializr_app.models.PersonalUserDTO;
import com.socializr.socializr_app.networking.marshall.ChangePasswordMarshall;
import com.socializr.socializr_app.networking.marshall.UpdateDescriptionMarshall;
import com.socializr.socializr_app.application.Session;
import com.socializr.socializr_app.networking.marshall.UploadImageMarshall;
import com.socializr.socializr_app.util.ImageEncoder;
import com.socializr.socializr_app.view.Settings.EditProfileActivity;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class EditProfilePresenter extends AbstractPresenter {
    EditProfileActivity activity;

    public EditProfilePresenter(EditProfileActivity activity) {
        super();
        this.activity = activity;
    }

    public void changePassword(String oldPass, String newPass, String confirmPass) {
        ChangePasswordMarshall marshall = new ChangePasswordMarshall(oldPass, newPass, confirmPass);
        restClient.changePassword(marshall, new Callback<String>() {
            @Override
            public void onResponse(Response<String> response, Retrofit retrofit) {
                if(response.isSuccess()) {
                    activity.toast("Password successfully changed");
                } else {
                }
            }

            @Override
            public void onFailure(Throwable t) {
                activity.toast("Server error");
            }
        });
    }

    public void updateDescription(String description) {
        UpdateDescriptionMarshall marshall = new UpdateDescriptionMarshall(description);

        restClient.updateDescription(marshall, new Callback<String>() {
            @Override
            public void onResponse(Response<String> response, Retrofit retrofit) {
                if(response.isSuccess()) {
                    restClient.getPersonalUser(new Callback<PersonalUserDTO>() {
                        @Override
                        public void onResponse(Response<PersonalUserDTO> response, Retrofit retrofit) {
                            if (response.isSuccess()) {
                                Session.getInstance().setUser(response.body());
                                activity.toast("Description successfully updated");
                                activity.onDescriptionUpdated();
                            }
                        }

                        @Override
                        public void onFailure(Throwable t) {
                        }
                    });
                } else {
                    activity.toast("Could not update description");
                }
            }

            @Override
            public void onFailure(Throwable t) {
                activity.toast("Could not update description server error");
            }
        });
    }

    public void uploadProfilePicture(Bitmap bitmap) {
        UploadImageMarshall marshall = new UploadImageMarshall(
                ImageEncoder.EncodeBitmap(bitmap),
                UploadImageMarshall.Type.PROFILE_PICTURE);

        restClient.uploadImage(marshall, new Callback<String>() {
            @Override
            public void onResponse(Response<String> response, Retrofit retrofit) {

            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }
}
