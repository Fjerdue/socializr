package com.socializr.socializr_app.view.Adapters;


import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.socializr.socializr_app.R;
import com.socializr.socializr_app.view.EventStack.EventInformation;
import com.socializr.socializr_app.view.Profile.AcceptProfileActivity;

import java.util.ArrayList;
import java.util.List;

public class AcceptListAdapter extends BaseAdapter {


    public class AcceptDummy {
        private int profileImage;
        private String profileName;

        AcceptDummy (int profileImage, String profileName){
            this.profileImage = profileImage;
            this.profileName = profileName;
        };

        public Drawable getProfileImage(Context context) {
            return context.getDrawable(profileImage);
        }

        public String getProfileName() {
            return profileName;
        }
    }


    private List<AcceptDummy> data;
    private Context context;
    private ImageView profileImage;
    private TextView profileName;
    private ImageButton reject, accept;
    private RelativeLayout acceptItem;


    public AcceptListAdapter(Context context){
        this.context = context;

        Resources r = context.getResources();

        data = new ArrayList<>();
        data.add(new AcceptDummy(R.drawable.david,"David"));
        data.add(new AcceptDummy(R.drawable.vennesa,"Vanessa"));
        data.add(new AcceptDummy(R.drawable.justin,"Justin"));
        data.add(new AcceptDummy(R.drawable.selena,"Selena"));
        data.add(new AcceptDummy(R.drawable.boxie,"Boxxy"));
        data.add(new AcceptDummy(R.drawable.zac,"Zac"));
    }



    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public AcceptDummy getItem(int i) {
        return data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {

        View v = view;
        if (v == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            v = inflater.inflate(R.layout.custom_accept, viewGroup, false);
        }

        profileImage = (ImageView) v.findViewById(R.id.accept_profile_imageview);
        profileName = (TextView) v.findViewById(R.id.accept_profile_name_textview);
        reject = (ImageButton) v.findViewById(R.id.accept_rejekt_button);
        accept = (ImageButton) v.findViewById(R.id.accept_accept_button);
        acceptItem = (RelativeLayout) v.findViewById(R.id.acceptItem);

        profileImage.setImageDrawable(getItem(i).getProfileImage(context));
        profileName.setText("" + getItem(i).getProfileName());

        reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context, "reject: " + getItem(i).getProfileName(), Toast.LENGTH_SHORT).show();
                data.remove(getItem(i));
                notifyDataSetChanged();
            }
        });

        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context, "accept: " + getItem(i).getProfileName(), Toast.LENGTH_SHORT).show();
                data.remove(getItem(i));
                notifyDataSetChanged();
            }
        });

        acceptItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, AcceptProfileActivity.class);
                context.startActivity(intent);
            }
        });


        return v;
    }


}
