package com.socializr.socializr_app.view.Settings;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.socializr.socializr_app.R;
import com.socializr.socializr_app.view.Adapters.CategoryAdapter;

public class EditFiltersActivity extends AppCompatActivity {

    private ListView categoriesLayout;
    private CategoryAdapter categoryAdaptor;
    private SeekBar radiusBar;
    private TextView radiusValue;
    private Button cancelButton, updateButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_filters);

        categoriesLayout = (ListView) findViewById(R.id.category_listview);
        radiusBar = (SeekBar) findViewById(R.id.radius_seekBar);
        radiusValue = (TextView) findViewById(R.id.radius_value_textView);
        cancelButton = (Button) findViewById(R.id.cancel_button);
        updateButton = (Button) findViewById(R.id.update_button);

        categoryAdaptor = new CategoryAdapter(this);
        categoriesLayout.setAdapter(categoryAdaptor);

        radiusValue.setText(String.valueOf(radiusBar.getProgress()+1)); // TODO GET FROM DATABASE

        radiusBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(){

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                radiusValue.setText(String.valueOf(progress+1));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                // TODO UPDATE USER / DB
            }
        });
    }
}
