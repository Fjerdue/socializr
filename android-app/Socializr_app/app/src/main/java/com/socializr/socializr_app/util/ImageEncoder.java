package com.socializr.socializr_app.util;

import android.graphics.Bitmap;
import android.util.Base64;

import java.io.ByteArrayOutputStream;

public class ImageEncoder {
    private ImageEncoder() {}

    public static String EncodeBitmap(Bitmap bitmap) {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 20, output);
        byte[] bytes = output.toByteArray();

        return  Base64.encodeToString(bytes, Base64.DEFAULT);
    }
}