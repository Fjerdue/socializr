package com.socializr.socializr_app.application;

import android.app.Application;
import android.graphics.Bitmap;

import com.socializr.socializr_app.models.Event;
import com.socializr.socializr_app.models.PersonalUserDTO;

public class Session extends Application {
    private static Session instance;

    private String jwt;
    private PersonalUserDTO user;
    private Bitmap profilePicture;
    private Event myEvent;
    private Bitmap myEventPicture;
    private Bitmap currentChatPicture;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    public static Session getInstance() {
        return instance;
    }

    public void setToken(String jwt) {
        this.jwt = jwt;
    }

    public String getToken() {
        return jwt;
    }

    public void setUser(PersonalUserDTO user) {
        this.user = user;
    }

    public PersonalUserDTO getUser() {
        return user;
    }

    public void setProfilePicture(Bitmap bitmap) {
        profilePicture = bitmap;
    }

    public Bitmap getProfilePicture() {
        return profilePicture;
    }

    public void setMyEvent(Event myEvent) {
        this.myEvent = myEvent;
    }

    public Event getMyEvent() {
        return myEvent;
    }

    public void setMyEventPicture(Bitmap myEventPicture) {
        this.myEventPicture = myEventPicture;
    }

    public Bitmap getMyEventPicture() {
        return  myEventPicture;
    }

    public void setCurrentChatPicture(Bitmap picture) {
        currentChatPicture = picture;

    }

    public Bitmap getCurrentChatPicture() {
        return currentChatPicture;
    }

    public void reset() {
        profilePicture = null;
        user = null;
        jwt = null;
        myEvent = null;
        myEventPicture = null;
    }
}
