package com.socializr.socializr_app.models;

public class UserDTO {

    protected String _id;
    protected String name;
    protected String description;
    protected long birthdate;
    protected String pictureURL;
    protected long rating;


    public UserDTO(String _id, String name, String description, long birthdate, String pictureURL, long rating) {
        this._id = _id;
        this.name = name;
        this.description = description;
        this.birthdate = birthdate;
        this.pictureURL = pictureURL;
        this.rating = rating;
    }

    public String getUserID() {
        return _id;
    }

    public void setUserID(String _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getBirthdate() {
        return this.birthdate;
    }

    public void setBirthdate(long birthdate) {
        this.birthdate = birthdate;
    }

    public String getPictureUrl() {
        return pictureURL;
    }

    public void setPictureUrl(String pictureURL) {
        this.pictureURL = pictureURL;
    }

    public long getRating() {
        return rating;
    }

    public void setRating(long rating) {
        this.rating = rating;
    }
}
