package com.socializr.socializr_app.view.Fragments;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.socializr.socializr_app.R;

public class ChangePasswordFragment extends DialogFragment implements View.OnClickListener {

    public interface OnSubmitListener {
        void onSubmitted(String oldPass, String newPass, String confirmPass);
    }

    private EditText oldPassword;
    private EditText newPassword;
    private EditText confirmPassword;
    private Button changePasswordButton;
    private Button cancelButton;
    OnSubmitListener callback;

    public void setCallback(OnSubmitListener callback) {
        this.callback = callback;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_change_password, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        View v = getView();

        oldPassword = (EditText) v.findViewById(R.id.oldPassword_edittext);
        newPassword = (EditText) v.findViewById(R.id.newPassword_edittext);
        confirmPassword = (EditText) v.findViewById(R.id.confirmPassword_edittext);

        changePasswordButton = (Button) v.findViewById(R.id.changePassword_button);
        changePasswordButton.setOnClickListener(this);

        cancelButton = (Button) v.findViewById(R.id.changePassword_cancel_button);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

    }

    @Override
    public void onClick(View v) {
        String oldPass = oldPassword.getText().toString();
        String newPass = newPassword.getText().toString();
        String confirmPass = confirmPassword.getText().toString();

        if(newPass.equals(confirmPass)) {
            callback.onSubmitted(oldPass, newPass, confirmPass);
            dismiss();
        } else {
            Toast t = Toast.makeText(getActivity(), "New passwords don't match", Toast.LENGTH_SHORT);
            t.show();
        }
    }
}
