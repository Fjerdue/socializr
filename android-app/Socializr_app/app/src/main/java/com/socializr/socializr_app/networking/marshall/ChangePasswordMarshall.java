package com.socializr.socializr_app.networking.marshall;

public class ChangePasswordMarshall {
    private String oldPass, newPass, confirmPass;

    public ChangePasswordMarshall(String oldPass, String newPass, String confirmPass) {
        this.oldPass = oldPass;
        this.newPass = newPass;
        this.confirmPass = confirmPass;
    }
}
