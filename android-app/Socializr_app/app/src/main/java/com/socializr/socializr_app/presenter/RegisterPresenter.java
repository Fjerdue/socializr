package com.socializr.socializr_app.presenter;

import android.util.Log;

import com.socializr.socializr_app.networking.marshall.RegisterMarshall;
import com.socializr.socializr_app.view.RegisterActivity;

import java.util.Date;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class RegisterPresenter extends AbstractPresenter {
    private static final String TAG = "RegisterPresenter";

    private RegisterActivity activity;

    public RegisterPresenter(RegisterActivity activity) {
        super();
        this.activity = activity;
    }

    public void register(String name, String email, String password, Date birthdate) {
        RegisterMarshall registerMarshall = new RegisterMarshall(name, email, password, birthdate.getTime());

        restClient.register(registerMarshall, new Callback<String>() {
            @Override
            public void onResponse(Response<String> response, Retrofit retrofit) {
                Log.d("HTTP", "register status " + response.code());

                if (response.isSuccess()) {
                    Log.d(TAG, "Register success");
                    activity.onRegisterSuccess();
                } else {
                    Log.d(TAG, "register failed with response " + response.code());
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.d("HTTP", "Register failed: " + t.getMessage());
            }
        });
    }
}
