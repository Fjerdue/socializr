package com.socializr.socializr_app.presenter;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.util.Log;

import com.socializr.socializr_app.broadcast_receivers.NotificationReceiver;
import com.socializr.socializr_app.models.Event;
import com.socializr.socializr_app.models.PersonalUserDTO;
import com.socializr.socializr_app.application.Session;
import com.socializr.socializr_app.view.MainActivity;

import java.io.InputStream;
import java.net.URL;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class MainPresenter extends AbstractPresenter {
    private static final String TAG = "MainPresenter";
    private MainActivity activity;

    public MainPresenter(MainActivity activity) {
        super();
        this.activity = activity;
        setupShouldSendNotificationChecker();
    }

    public void getPersonalUser() {
        Log.d("HTTP", "getting authed user");

        restClient.getPersonalUser(new Callback<PersonalUserDTO>() {
            @Override
            public void onResponse(Response<PersonalUserDTO> response, Retrofit retrofit) {
                Log.d(TAG, "getPersonalUser called w/ status " + response.code());

                if (response.isSuccess()) {
                    Session.getInstance().setUser(response.body());

                    new AsyncTask<Void, Void, Void>() {
                        @Override
                        protected Void doInBackground(Void[] params) {
                            try {
                                Bitmap bitmap = BitmapFactory.decodeStream((InputStream)new URL("http://danielpc.dk:3000/" + Session.getInstance().getUser().getPictureUrl()).getContent());
                                Session.getInstance().setProfilePicture(bitmap);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void o) {
                            super.onPostExecute(o);
                            activity.initialize();
                        }
                    }.execute();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                System.err.println(t.getMessage());
            }
        });
    }

    public void initMyEvent() {
        restClient.getMyEvent(new Callback<Event>() {
            @Override
            public void onResponse(Response<Event> response, Retrofit retrofit) {
                if(response.isSuccess()) {
                    Session.getInstance().setMyEvent(response.body());

                }
            }

            @Override
            public void onFailure(Throwable t) { System.err.println(t.getMessage()); }
        });
    }

    private void setupShouldSendNotificationChecker() {
        Intent i = new Intent(activity, NotificationReceiver.class);

        PendingIntent pi = PendingIntent.getBroadcast(activity, 420, i, 0);

        long startTime = SystemClock.elapsedRealtime() + 60 * 1000;

        AlarmManager am = (AlarmManager)activity.getSystemService(Context.ALARM_SERVICE);
        am.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, startTime, 60 * 1000, pi);
    }
}
