package com.socializr.socializr_app.networking.marshall;

public class GetEventsForCardStackMarshall {
    private int noOfEvents;

    public GetEventsForCardStackMarshall(int noOfEvents) {
        this.noOfEvents = noOfEvents;
    }

    public int getNoOfEvents() {
        return noOfEvents;
    }

    public void setNoOfEvents(int noOfEvents) {
        this.noOfEvents = noOfEvents;
    }
}
