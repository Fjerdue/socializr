package com.socializr.socializr_app.custom_view;

import android.content.Context;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;

public class AutoClickOnFocusEditText extends EditText implements View.OnFocusChangeListener, View.OnKeyListener {
    public AutoClickOnFocusEditText(Context context) {
        super(context);
        init();
    }

    public AutoClickOnFocusEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public AutoClickOnFocusEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public AutoClickOnFocusEditText(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        setOnFocusChangeListener(this);
        setOnKeyListener(this);
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if(hasFocus) {
            long down = SystemClock.uptimeMillis();
            long time = down + 100;

            MotionEvent e = MotionEvent.obtain(down, time, MotionEvent.ACTION_DOWN, 0, 0, 0);

            v.dispatchTouchEvent(e);
        }
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        return keyCode != KeyEvent.KEYCODE_TAB;
    }
}
