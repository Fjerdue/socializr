package com.socializr.socializr_app.networking;

public class CreateEventMarshall {
    private String eventTitle, eventAddress, eventCategory;
    private int seats;
    private long eventDateTime, eventDeadlineDateTime, minAge, maxAge;

    public CreateEventMarshall(String eventTitle, String eventAddress, String eventCategory,
                               int seats, long eventDateTime, long eventDeadlineDateTime,
                               long minAge, long maxAge) {
        this.eventTitle = eventTitle;
        this.eventAddress = eventAddress;
        this.eventCategory = eventCategory;
        this.seats = seats;
        this.eventDateTime = eventDateTime;
        this.eventDeadlineDateTime = eventDeadlineDateTime;
        this.minAge = minAge;
        this.maxAge = maxAge;
    }

    public String getEventTitle() {
        return eventTitle;
    }

    public void setEventTitle(String eventTitle) {
        this.eventTitle = eventTitle;
    }

    public String getEventAddress() {
        return eventAddress;
    }

    public void setEventAddress(String eventAddress) {
        this.eventAddress = eventAddress;
    }

    public String getEventCategory() {
        return eventCategory;
    }

    public void setEventCategory(String eventCategory) {
        this.eventCategory = eventCategory;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public long getEventDateTime() {
        return eventDateTime;
    }

    public void setEventDateTime(long eventDateTime) {
        this.eventDateTime = eventDateTime;
    }

    public long getEventDeadlineDateTime() {
        return eventDeadlineDateTime;
    }

    public void setEventDeadlineDateTime(long eventDeadlineDateTime) {
        this.eventDeadlineDateTime = eventDeadlineDateTime;
    }

    public long getMinAge() {
        return minAge;
    }

    public void setMinAge(long minAge) {
        this.minAge = minAge;
    }

    public long getMaxAge() {
        return maxAge;
    }

    public void setMaxAge(long maxAge) {
        this.maxAge = maxAge;
    }

    @Override
    public String toString() {
        return "CreateEventMarshall{" +
                "eventTitle='" + eventTitle + '\'' +
                ", eventAddress='" + eventAddress + '\'' +
                ", eventCategory='" + eventCategory + '\'' +
                ", seats=" + seats +
                ", eventDateTime=" + eventDateTime +
                ", eventDeadlineDateTime=" + eventDeadlineDateTime +
                ", minAge=" + minAge +
                ", maxAge=" + maxAge +
                '}';
    }
}