package com.socializr.socializr_app.view.Profile;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.socializr.socializr_app.R;

public class ProfileStrangerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_stranger);
    }
}
