package com.socializr.socializr_app.view.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Switch;

import com.socializr.socializr_app.R;



public class CategoryAdapter extends BaseAdapter {
    private String[] categories;
    private LayoutInflater switchInf;

    public CategoryAdapter(Context c){
        // TODO GET FROM SERVER
        categories = new String[3];
        categories[0] = "Sport";
        categories[1] = "Party";
        categories[2] = "Chill";
        switchInf = LayoutInflater.from(c);

    }

    @Override
    public int getCount() {
        return categories.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Switch androidSwitch;

        if (convertView == null) {
            androidSwitch = (Switch)switchInf.inflate(R.layout.category_switch, parent, false);;
        } else {
            androidSwitch = (Switch) convertView;
        }

        androidSwitch.setText(categories[position]);

        return androidSwitch;
    }
}
