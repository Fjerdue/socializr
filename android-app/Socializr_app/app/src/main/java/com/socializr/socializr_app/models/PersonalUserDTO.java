package com.socializr.socializr_app.models;

import android.location.Location;

import com.socializr.socializr_app.util.AgeCalculator;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class PersonalUserDTO extends UserDTO {

    private ArrayList<String> categories;
    private int radius;
    private android.location.Location location;
    private ArrayList<String> eventsParticipating;
    private String event;

    public PersonalUserDTO(String _id,
                           String name,
                           String description,
                           long birthdate,
                           String pictureURL,
                           long rating,
                           ArrayList<String> categories,
                           int radius,
                           ArrayList<String> eventsParticipating,
                           String event) {

        super(_id, name, description, birthdate, pictureURL, rating);

        this.categories = categories;
        this.radius = radius;
        this.eventsParticipating = eventsParticipating;
        this.event = event;
    }

    public ArrayList<String> getCategoryPreference() {
        return categories;
    }

    public void setCategoryPreference(ArrayList<String> categories) {
        this.categories = categories;
    }

    public int getRadiusKm() {
        return radius;
    }

    public void setRadiusKm(int radius) {
        this.radius = radius;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public ArrayList<String> getEventsParticipatingInIDs() {
        return eventsParticipating;
    }

    public void setEventsParticipatingInIDs(ArrayList<String> eventsParticipating) {
        this.eventsParticipating = eventsParticipating;
    }

    public String getEventAdministratingID() {
        return event;
    }

    public void setEventAdministratingID(String event) {
        this.event = event;
    }

    public int getAge() {
        return AgeCalculator.getAgeFromDate(new Date(this.birthdate));
    }

    @Override
    public String toString() {
        return "PersonalUserDTO{" +
                "_id='" + _id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", birthdate=" + birthdate +
                ", pictureURL='" + pictureURL + '\'' +
                ", rating=" + rating +
                ", event='" + event + '\'' +
                ", eventsParticipating=" + eventsParticipating +
                ", location=" + location +
                ", radius=" + radius +
                ", categories=" + categories +
                '}';
    }
}
