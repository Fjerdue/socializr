package com.socializr.socializr_app.view.MyEvents;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.socializr.socializr_app.R;
import com.socializr.socializr_app.application.Session;
import com.socializr.socializr_app.view.Profile.ChatProfileStrangerActivity;
import com.socializr.socializr_app.view.Profile.ProfileStrangerActivity;

public class ChatActivity extends AppCompatActivity {
    private ImageView eventImage;
    private LinearLayout llGuest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        eventImage = (ImageView) findViewById(R.id.chat_event_image);
        eventImage.setImageBitmap(Session.getInstance().getCurrentChatPicture());

        llGuest = (LinearLayout) findViewById(R.id.llGuests);

        eventImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), EventInformationAcceptedActivity.class));
            }
        });

        llGuest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), ChatProfileStrangerActivity.class));
            }
        });
    }
}
