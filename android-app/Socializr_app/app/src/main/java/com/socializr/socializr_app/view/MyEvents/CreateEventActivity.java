package com.socializr.socializr_app.view.MyEvents;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.socializr.socializr_app.R;
import com.socializr.socializr_app.application.Session;
import com.socializr.socializr_app.custom_view.SquareImageView;
import com.socializr.socializr_app.models.Event;
import com.socializr.socializr_app.networking.CreateEventMarshall;
import com.socializr.socializr_app.networking.marshall.UploadImageMarshall;
import com.socializr.socializr_app.presenter.CreateEventPresenter;
import com.socializr.socializr_app.presenter.Date;
import com.socializr.socializr_app.util.ImageEncoder;
import com.socializr.socializr_app.view.CameraActivity;

import org.florescu.android.rangeseekbar.RangeSeekBar;

import java.util.Calendar;

import retrofit.Response;

public class CreateEventActivity extends CameraActivity {
    private RangeSeekBar seekbar;

    public static final String TAG = "CreateEventActivity";
    private Spinner dropdownCategoriesView;
    private Button cancelButton;
    private Button createButton;
    private EditText addressView;
    private EditText titleView;
    private EditText dateView;
    private EditText seatsView;
    private EditText deadlineView;
    private EditText dateTimeView;
    private EditText deadlineTimeView;
    private FloatingActionButton fab;
    private SquareImageView eventPicture;

    private Calendar calendar,
            eventDate,
            eventDeadlineDate,
            maxDeadlineDate;

    private int mDay;
    private int mMonth;
    private int mYear;
    private int mHour;
    private int mMinutes;
    private Bitmap picture;

    private CreateEventPresenter presenter;

    private String[] categories;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_event);

        presenter = new CreateEventPresenter(this);

        seekbar = (RangeSeekBar) findViewById(R.id.rangeSeekbar);

        dropdownCategoriesView = (Spinner) findViewById(R.id.createEventCategory_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.categories_array, android.R.layout.simple_spinner_item);
        dropdownCategoriesView.setAdapter(adapter);

        titleView = (EditText) findViewById(R.id.createEventTitle_editText);
        addressView = (EditText) findViewById(R.id.createEventAddress_editText);
        seatsView = (EditText) findViewById(R.id.createEventSeats_editText);
        dateView = (EditText) findViewById(R.id.createEventDate_editText);
        deadlineView = (EditText) findViewById(R.id.createEventDeadline_editText);
        dateTimeView = (EditText) findViewById(R.id.createEventDateTime_editText);
        deadlineTimeView = (EditText) findViewById(R.id.createEventDeadlineTime_editText);
        fab = (FloatingActionButton)findViewById(R.id.fab);
        eventPicture = (SquareImageView)findViewById(R.id.eventPicture_imageview);

        calendar = Calendar.getInstance();
        mYear = calendar.get(Calendar.YEAR);
        mMonth = calendar.get(Calendar.MONTH);
        mDay = calendar.get(Calendar.DAY_OF_MONTH);
        mHour = calendar.get(Calendar.HOUR_OF_DAY);
        mMinutes = calendar.get(Calendar.MINUTE);

        eventDate = Calendar.getInstance();
        eventDeadlineDate = Calendar.getInstance();

        picture = null;

        seekbar.setTextAboveThumbsColor(Color.BLACK);
        seekbar.setRangeValues(16, 100);

        dateView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() != MotionEvent.ACTION_DOWN)
                    return false;

                DatePickerDialog mDatePicker = new DatePickerDialog(CreateEventActivity.this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedYear, int selectedMonth, int selectedDay) {
                        eventDate.set(selectedYear, selectedMonth, selectedDay);
                        dateView.setText(selectedDay + "/" + (selectedMonth+1) + "/" + selectedYear);
                    }
                }, mYear, mMonth, mDay);
                mDatePicker.getDatePicker().setMinDate(calendar.getTimeInMillis());
                mDatePicker.show();
                return true;
            }
        });

        dateTimeView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() != MotionEvent.ACTION_DOWN)
                    return false;

                TimePickerDialog mTimePicker = new TimePickerDialog(CreateEventActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    public void onTimeSet(TimePicker timepicker, int hourOfDay, int minute)  {
                        eventDate.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        eventDate.set(Calendar.MINUTE, minute);
                        dateTimeView.setText(hourOfDay + ":" + minute);
                    }
                }, mHour, mMinutes, DateFormat.is24HourFormat(CreateEventActivity.this));
                mTimePicker.show();
                return true;
            }
        });

        deadlineView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() != MotionEvent.ACTION_DOWN)
                    return false;

                DatePickerDialog mDatePicker = new DatePickerDialog(CreateEventActivity.this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedYear, int selectedMonth, int selectedDay) {
                        eventDeadlineDate.set(selectedYear, selectedMonth, selectedDay);
                        deadlineView.setText(selectedDay + "/" + (selectedMonth+1) + "/" + selectedYear);
                    }
                }, mYear, mMonth, mDay);
                mDatePicker.show();
                mDatePicker.getDatePicker().setMinDate(calendar.getTimeInMillis());

                maxDeadlineDate = eventDate;

                if (maxDeadlineDate.get(Calendar.DAY_OF_YEAR) != calendar.get(Calendar.DAY_OF_YEAR)
                        || maxDeadlineDate.get(Calendar.YEAR) != calendar.get(Calendar.YEAR)) {
                    maxDeadlineDate.add(Calendar.DAY_OF_YEAR, -1);
                }

                mDatePicker.getDatePicker().setMaxDate(maxDeadlineDate.getTimeInMillis());

                return true;
            }
        });

        deadlineTimeView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() != MotionEvent.ACTION_DOWN)
                    return false;

                TimePickerDialog mTimePicker = new TimePickerDialog(CreateEventActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    public void onTimeSet(TimePicker timepicker, int hourOfDay, int minute)  {
                        eventDeadlineDate.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        eventDeadlineDate.set(Calendar.MINUTE, minute);
                        deadlineTimeView.setText(hourOfDay + ":" + minute);
                    }
                }, mHour, mMinutes, DateFormat.is24HourFormat(CreateEventActivity.this));
                mTimePicker.show();
                return true;
            }
        });

        cancelButton = (Button) findViewById(R.id.createEventCancel_button);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        createButton = (Button) findViewById(R.id.createEventCreate_button);
        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!validateFieldsNotEmpty()) {
                    return;
                }

                String eventTitleText = titleView.getText().toString();
                String eventCategoryText = dropdownCategoriesView.getSelectedItem().toString();
                String eventAddressText = addressView.getText().toString();
                long eventDateCalendarTimestamp = eventDate.getTimeInMillis();
                long eventDeadlineDateTimestamp = eventDeadlineDate.getTimeInMillis();
                int minAge = (int) seekbar.getSelectedMinValue();
                int maxAge = (int) seekbar.getSelectedMaxValue();
                int seats = Integer.parseInt(seatsView.getText().toString());

                CreateEventMarshall marshall = new CreateEventMarshall(
                        eventTitleText, eventAddressText, eventCategoryText, seats,
                        eventDateCalendarTimestamp, eventDeadlineDateTimestamp, minAge, maxAge);

                Log.d(TAG, marshall.toString());
                System.out.println(marshall);

                UploadImageMarshall image = new UploadImageMarshall(ImageEncoder.EncodeBitmap(picture), UploadImageMarshall.Type.EVENT_PICTURE);

                Session.getInstance().setMyEventPicture(picture);

                presenter.createEvent(marshall, image);
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                choosePicture();
            }
        });
    }

    private boolean validateFieldsNotEmpty() {


        if(picture == null){
            Toast.makeText( this, "Please upload a picture for your event", Toast.LENGTH_SHORT ).show();
            return false;
        }

        if(dropdownCategoriesView.getSelectedItemPosition() == 0){
            Toast.makeText( this, "Please choose a category", Toast.LENGTH_SHORT ).show();
            return  false;
        }

        if(titleView.getText().toString().isEmpty()){
            Toast.makeText( this, "Title must be filled", Toast.LENGTH_SHORT ).show();
            return false;
        }

        if(seatsView.getText().toString().isEmpty()){
            Toast.makeText( this, "Seats must be filled", Toast.LENGTH_SHORT ).show();
            return false;
        }

        if(dateTimeView.getText().toString().isEmpty()){
            Toast.makeText( this, "Date must be filled", Toast.LENGTH_SHORT ).show();
            return false;
        }

        if(deadlineView.getText().toString().isEmpty()){
            Toast.makeText( this, "Deadline must be filled", Toast.LENGTH_SHORT ).show();
            return false;
        }

        if(deadlineTimeView.getText().toString().isEmpty()){
            Toast.makeText( this, "Deadline time must be filled", Toast.LENGTH_SHORT ).show();
            return false;
        }


        if(addressView.getText().toString().isEmpty()){
            Toast.makeText( this, "Address must be filled", Toast.LENGTH_SHORT ).show();
            return false;
        }


        return true;

    }

    private long getUnixTimestampFromDate(Date d) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR,          d.getYear());
        cal.set(Calendar.MONTH,         d.getMonth());
        cal.set(Calendar.DAY_OF_MONTH,  d.getDay());
        cal.set(Calendar.HOUR,          d.getHour());
        cal.set(Calendar.MINUTE,        d.getMinute());
        return cal.getTimeInMillis();
    }

    public void onCreateEventSuccess() {
        Toast.makeText(this, "Event created", Toast.LENGTH_LONG).show();
        finish();
    }

    public void onCreateEventFailed(Response<Event> response) {
        Toast.makeText(this, "Error", Toast.LENGTH_LONG).show();
    }

    public void onPictureUploadFailed(Response<String> response) {
        Toast.makeText(this, "Picure error", Toast.LENGTH_LONG).show();
    }

    public void onLoginError(Throwable t) {
        Toast.makeText(this, "Server error", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onPicture(Bitmap bitmap) {
        picture = bitmap;
        eventPicture.setImageBitmap(bitmap);
    }
}
