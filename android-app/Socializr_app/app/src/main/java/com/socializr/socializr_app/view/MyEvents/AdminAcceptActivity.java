package com.socializr.socializr_app.view.MyEvents;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Adapter;
import android.widget.ListView;

import com.socializr.socializr_app.R;
import com.socializr.socializr_app.view.Adapters.AcceptListAdapter;

public class AdminAcceptActivity extends AppCompatActivity {

    private ListView listView;
    private AcceptListAdapter acceptListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_accept);

        listView = (ListView) findViewById(R.id.admin_accept_listview);

        acceptListAdapter = new AcceptListAdapter(this);

        listView.setAdapter(acceptListAdapter);
    }
}
