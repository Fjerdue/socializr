package com.socializr.socializr_app.networking;

import com.socializr.socializr_app.models.Event;
import com.socializr.socializr_app.models.PersonalUserDTO;
import com.socializr.socializr_app.networking.marshall.AddUserToEventQueueMarshall;
import com.socializr.socializr_app.networking.marshall.ChangePasswordMarshall;
import com.socializr.socializr_app.networking.marshall.GetEventsForCardStackMarshall;
import com.socializr.socializr_app.networking.marshall.LoginMarshall;
import com.socializr.socializr_app.networking.marshall.RegisterMarshall;
import com.socializr.socializr_app.networking.marshall.UpdateDescriptionMarshall;
import com.socializr.socializr_app.networking.marshall.UploadImageMarshall;

import java.util.List;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.POST;

interface SocializrAPI {
    @POST("login")
    Call<String> login(@Body LoginMarshall body);

    @POST("register")
    Call<String> register(@Body RegisterMarshall body);

    @POST("updateDescription")
    Call<String> updateDescription(@Header ("x-access-token") String jwt, @Body UpdateDescriptionMarshall body);

    @POST("changePassword")
    Call<String> changePassword(@Header("x-access-token") String jwt, @Body ChangePasswordMarshall body);

    @GET("getPersonalUser")
    Call<PersonalUserDTO> getUser(@Header("x-access-token") String jwt);

    @POST("createEvent")
    Call<Event> createEvent(@Header("x-access-token") String jwt,
                            @Body CreateEventMarshall body);

    @POST("uploadImage")
    Call<String> uploadImage(@Header("x-access-token")String jwt, @Body UploadImageMarshall body);

    @POST("addUserToEventQueue")
    Call<String> addUserToEventQueue(@Header("x-access-token") String jwt,
                                     @Body AddUserToEventQueueMarshall body);

    @POST("getEventsForCardStack")
    Call<List<Event>> getEventsForCardStack(@Header("x-access-token") String jwt,
                                            @Body GetEventsForCardStackMarshall body);

    @GET("getMyEvent")
    Call<Event> getMyEvent(@Header("x-access-token") String jwt);
}
