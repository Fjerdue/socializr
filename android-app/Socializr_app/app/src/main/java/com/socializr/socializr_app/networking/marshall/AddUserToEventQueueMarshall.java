package com.socializr.socializr_app.networking.marshall;

public class AddUserToEventQueueMarshall {
    private String eventId;

    public AddUserToEventQueueMarshall(String eventId) {
        this.eventId = eventId;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }
}
