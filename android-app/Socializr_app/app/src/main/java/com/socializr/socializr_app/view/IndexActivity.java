package com.socializr.socializr_app.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.crashlytics.android.Crashlytics;
import com.socializr.socializr_app.R;
import com.socializr.socializr_app.config.Config;

import io.fabric.sdk.android.Fabric;

public class IndexActivity extends AppCompatActivity {
    private Button login, register;
    private IndexActivity that = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_index);

        if (!Config.EMULATOR) {
            Fabric.with(this, new Crashlytics());
        }

        this.login = (Button) findViewById(R.id.login_button);
        this.register = (Button) findViewById(R.id.emailSignin_button);

        register.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                startActivity(new Intent(that, RegisterActivity.class));

            }
        });

        login.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                startActivity(new Intent(that, LoginActivity.class));

            }
        });
    }


}
