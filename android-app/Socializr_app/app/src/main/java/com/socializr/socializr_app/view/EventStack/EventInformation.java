package com.socializr.socializr_app.view.EventStack;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import com.socializr.socializr_app.R;
import com.socializr.socializr_app.view.Profile.ProfileStrangerActivity;

public class EventInformation extends AppCompatActivity {
    private ImageButton buttonSwipeLeft;
    private ImageButton getButtonSwipeRight;
    private LinearLayout llProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_information);

        buttonSwipeLeft = (ImageButton) findViewById(R.id.swipe_left_info_button);
        getButtonSwipeRight = (ImageButton) findViewById(R.id.swipe_right_info_button);
        llProfile = (LinearLayout) findViewById(R.id.ll_profile_event_information);

        buttonSwipeLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            //TODO swipe on card
            public void onClick(View view) {
                finish();
            }
        });

        getButtonSwipeRight.setOnClickListener(new View.OnClickListener() {
            @Override
            //TODO swipe on card
            public void onClick(View view) {
                finish();
            }
        });

        llProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), ProfileStrangerActivity.class));
            }
        });
    }



}
