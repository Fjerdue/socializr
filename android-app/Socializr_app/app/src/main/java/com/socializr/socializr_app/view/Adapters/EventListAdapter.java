package com.socializr.socializr_app.view.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.socializr.socializr_app.R;
import com.socializr.socializr_app.application.Session;
import com.socializr.socializr_app.custom_view.RoundedImageView;
import com.socializr.socializr_app.view.MyEvents.ChatActivity;

import java.util.ArrayList;
import java.util.List;

public class EventListAdapter extends BaseAdapter {


    public class EventDummy{
        private String id;
        private int eventPicture;
        private String eventName;

        EventDummy(String id, int eventPicture, String eventName){
            this.id = id;
            this.eventPicture = eventPicture;
            this.eventName = eventName;
        }

        public String getEventName() {
            return eventName;
        }

        public Drawable getEventPicture(Context context) { return context.getDrawable(eventPicture); }

        public Bitmap getEventPictureAsBitmap(Context context) { return ((BitmapDrawable)getEventPicture(context)).getBitmap(); }

        public String getId() {
            return id;
        }
    }

    private List<EventDummy> data;
    private Context context;
    private LinearLayout eventLayout;
    private RoundedImageView imageView;
    private TextView textView;
    private ImageView imageEvent;

    public EventListAdapter (Context context) {

        this.context = context;

        data = new ArrayList<>();

        data.add(new EventDummy("12", R.drawable.app_programming, "App programmering"));
        data.add(new EventDummy("13", R.drawable.hygge_hos_mig, "Hygge hos mig"));
        data.add(new EventDummy("14", R.drawable.dart, "Dartturnering"));
        data.add(new EventDummy("26", R.drawable.pizza, "Flytning med pizza"));
        data.add(new EventDummy("15", R.drawable.speed, "Speeddating"));
        data.add(new EventDummy("16", R.drawable.is, "Is på havnen"));
        data.add(new EventDummy("17", R.drawable.fest_grill, "Fest på grillen"));
        data.add(new EventDummy("18", R.drawable.kantinen, "Frokost i kantinen"));
        data.add(new EventDummy("19", R.drawable.running, "Løbetur rundt på campus"));
        data.add(new EventDummy("20", R.drawable.fodbold, "Fodbold på plænen"));
        data.add(new EventDummy("21", R.drawable.vandpyt, "Vandpytte-springning"));
        data.add(new EventDummy("22", R.drawable.kaffe, "Kaffe"));
        data.add(new EventDummy("23", R.drawable.cafe, "Cafe hygge"));
        data.add(new EventDummy("24", R.drawable.shopping, "Shopping"));
        data.add(new EventDummy("25", R.drawable.tegning, "Lær at tegne"));
    }


    @Override
    public int getCount() {return data.size();}

    @Override
    public EventDummy getItem(int i) {
        return data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, final View view, ViewGroup viewGroup) {
        View v = view;
        if (v == null) {
            LayoutInflater inflater =  LayoutInflater.from(context);
            v = inflater.inflate(R.layout.custom_event, viewGroup, false);
        }
        eventLayout = (LinearLayout) v.findViewById(R.id.llEventLayout);
        textView = (TextView) v.findViewById(R.id.textViewEvent);
        imageView = (RoundedImageView) v.findViewById(R.id.imageEvent);

        textView.setText(getItem(i).getEventName());
        textView.setTextSize(20);

        imageView.setImageDrawable(getItem(i).getEventPicture(context));

        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent chatIntent = new Intent(context, ChatActivity.class);
                Session.getInstance().setCurrentChatPicture(getItem(i).getEventPictureAsBitmap(context));
                context.startActivity(chatIntent);
            }
        });

        return v;
    }
}