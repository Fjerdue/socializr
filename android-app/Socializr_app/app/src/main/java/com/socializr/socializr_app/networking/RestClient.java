package com.socializr.socializr_app.networking;

import com.socializr.socializr_app.application.Session;
import com.socializr.socializr_app.models.Event;
import com.socializr.socializr_app.models.PersonalUserDTO;
import com.socializr.socializr_app.networking.marshall.AddUserToEventQueueMarshall;
import com.socializr.socializr_app.networking.marshall.ChangePasswordMarshall;
import com.socializr.socializr_app.networking.marshall.GetEventsForCardStackMarshall;
import com.socializr.socializr_app.networking.marshall.LoginMarshall;
import com.socializr.socializr_app.networking.marshall.RegisterMarshall;
import com.socializr.socializr_app.networking.marshall.UpdateDescriptionMarshall;
import com.socializr.socializr_app.networking.marshall.UploadImageMarshall;

import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

public class RestClient {
    private static RestClient instance = null;

    private static final String BASE_URL = "http://danielpc.dk:3000/";
    //private static final String BASE_URL = "http://10.0.2.2:3000/"; //for local testing

    private Retrofit retrofit;
    private SocializrAPI socializrAPI;

    private RestClient() {
        this.retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        this.socializrAPI = retrofit.create(SocializrAPI.class);
    }

    public static RestClient getInstance() {
        if (instance == null) {
            instance = new RestClient();
        }

        return instance;
    }

    public void login(LoginMarshall loginMarshall, Callback<String> callback) {
        Call<String> call = socializrAPI.login(loginMarshall);
        call.enqueue(callback);
    }

    public void register(RegisterMarshall registerMarshall, Callback<String> callback) {
        Call<String> call = socializrAPI.register(registerMarshall);
        call.enqueue(callback);
    }

    public void updateDescription(UpdateDescriptionMarshall updateDescriptionMarshall, Callback<String> callback) {
        Call<String> call = socializrAPI.updateDescription(Session.getInstance().getToken(), updateDescriptionMarshall);
        call.enqueue(callback);
    }

    public void changePassword(ChangePasswordMarshall changePasswordMarshall, Callback<String> callback) {
        Call<String> call = socializrAPI.changePassword(Session.getInstance().getToken(), changePasswordMarshall);
        call.enqueue(callback);
    }

    public void getPersonalUser(Callback<PersonalUserDTO> callback) {
        Call<PersonalUserDTO> call = socializrAPI.getUser(Session.getInstance().getToken());
        call.enqueue(callback);
    }

    public void createEvent(CreateEventMarshall createEventMarshall, Callback<Event> callback) {
        Call<Event> call = socializrAPI.createEvent(Session.getInstance().getToken(), createEventMarshall);
        call.enqueue(callback);
    }

    public void uploadImage(UploadImageMarshall uploadImageMarshall, Callback<String> callback) {
        Call<String> call = socializrAPI.uploadImage(Session.getInstance().getToken(), uploadImageMarshall);
        call.enqueue(callback);
    }

    public void addUserToEventQueue(AddUserToEventQueueMarshall marshall, Callback<String> callback) {
        Call<String> call = socializrAPI.addUserToEventQueue(Session.getInstance().getToken(), marshall);
        call.enqueue(callback);
    }

    public void getEventsForCardStack(GetEventsForCardStackMarshall marshall, Callback<List<Event>> callback) {
        Call<List<Event>> call = socializrAPI.getEventsForCardStack(Session.getInstance().getToken(), marshall);
        call.enqueue(callback);
    }

    public void getMyEvent(Callback<Event> callback) {
        Call<Event> call = socializrAPI.getMyEvent(Session.getInstance().getToken());
        call.enqueue(callback);
    }
}
