package com.socializr.socializr_app.view.Adapters;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.socializr.socializr_app.R;
import com.socializr.socializr_app.view.EventStack.EventInformation;

import java.util.ArrayList;
import java.util.List;

public class SwipeDeckAdapter extends BaseAdapter {
    public class StackDummy{
        private String id;
        private String date;
        private int distance;
        private String title;
        private int seats;
        private int eventPicture;
        private int eventCategory;
        private float rating;

        StackDummy(String id, String date, int seats, String title, int distance, int eventPicture, int eventCategory, float rating){
            this.id = id;
            this.date = date;
            this.distance = distance;
            this.title = title;
            this.seats = seats;
            this.eventPicture = eventPicture;
            this.eventCategory = eventCategory;
            this.rating = rating;
        }

        public String getId() {
            return id;
        }

        public String getDate() {
            return date;
        }

        public int getDistance() {
            return distance;
        }

        public String getTitle() {
            return title;
        }

        public int getSeats() {
            return seats;
        }

        public Drawable getEventPicture(Context context) {
            return context.getDrawable(eventPicture);
        }

        public Drawable getEventCategory(Context context) {
            return context.getDrawable(eventCategory);
        }

        public float getRating() {
            return rating;
        }
    }


    private List<StackDummy> data;
    private Context context;
    private TextView eventTitle;
    private TextView eventDate;
    private TextView eventSeats;
    private TextView eventDistance;
    private ImageView eventPicture;
    private ImageView eventCategory;
    private RatingBar eventRating;

    public SwipeDeckAdapter(Context context) {
        this.context = context;

        Resources r = context.getResources();
        data = new ArrayList<>();
        data.add( new StackDummy("1", "4/2-2017 13:00", 8, "Strandfest", 25,  R.drawable.beach, R.mipmap.ic_local_bar_black_48dp, 1.5f ));
        data.add( new StackDummy("1", "26/01-2017 15:00", 10, "Kaffehygge", 11,  R.drawable.coffee, R.mipmap.ic_free_breakfast_black_48dp, 5.0f ));
        data.add( new StackDummy("1", "15/02-2017 20:00", 25, "Fredagskoncert", 42,  R.drawable.eventdummy, R.mipmap.ic_local_bar_black_48dp, 4.2f ));
        data.add( new StackDummy("1", "27/01-2017 15:30", 22, "Fodbold med mig", 25,  R.drawable.football, R.mipmap.ic_directions_run_black_48dp, 3.2f ));
        data.add( new StackDummy("1", "24/01-2017 20:00", 5, "Racing", 54,  R.drawable.gokart, R.mipmap.ic_directions_run_black_48dp, 2.0f ));
        data.add( new StackDummy("1", "15/07-2017 20:00", 20, "Kæmpe grillfest", 11,  R.drawable.grillparty, R.mipmap.ic_local_bar_black_48dp, 2.6f ));
    }


    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public StackDummy getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater inflater =  LayoutInflater.from(context);
            v = inflater.inflate(R.layout.custom_cardview, parent, false);
        }

        ImageView imageView = (ImageView) v.findViewById(R.id.eventPicture_image );
        eventTitle = (TextView) v.findViewById(R.id.event_title_textView);
        eventDate = (TextView) v.findViewById(R.id.eventDate_textView);
        eventSeats = (TextView) v.findViewById( R.id.eventSeats_textView );
        eventDistance = (TextView) v.findViewById( R.id.eventDistance_textView );
        eventPicture = (ImageView) v.findViewById( R.id.eventPicture_image );
        eventCategory = (ImageView) v.findViewById( R.id.eventCategory_imageView );
        eventRating = (RatingBar) v.findViewById( R.id.eventratingBar_ratingbar );

        eventTitle.setText(getItem(position).getTitle());
        eventDate.setText( getItem( position).getDate() );
        eventSeats.setText("" + getItem( position ).getSeats());
        eventDistance.setText("" +  getItem( position ).getDistance() + " km");
        eventPicture.setImageDrawable( getItem( position ).getEventPicture(context) );
        eventCategory.setImageDrawable( getItem( position ).getEventCategory(context) );
        eventRating.setRating( getItem( position ).getRating() );

        // TODO open for selected id
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, EventInformation.class);
                context.startActivity(intent);
            }
        });
        return v;
    }
}
