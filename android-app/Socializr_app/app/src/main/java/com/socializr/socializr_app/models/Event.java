package com.socializr.socializr_app.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Event {
    @SerializedName("_id")
    private String id;

    private String title,
            category,
            pictureURL,
            address;

    private Long timeCreated,
            timeStarting,
            deadline,
            maxParticipants,
            minAge,
            maxAge;

    private User admin;
    private List<User> participants, queue;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getPictureURL() {
        return pictureURL;
    }

    public void setPictureURL(String pictureURL) {
        this.pictureURL = pictureURL;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getTimeCreated() {
        return timeCreated;
    }

    public void setTimeCreated(Long timeCreated) {
        this.timeCreated = timeCreated;
    }

    public Long getTimeStarting() {
        return timeStarting;
    }

    public void setTimeStarting(Long timeStarting) {
        this.timeStarting = timeStarting;
    }

    public Long getDeadline() {
        return deadline;
    }

    public void setDeadline(Long deadline) {
        this.deadline = deadline;
    }

    public Long getMaxParticipants() {
        return maxParticipants;
    }

    public void setMaxParticipants(Long maxParticipants) {
        this.maxParticipants = maxParticipants;
    }

    public Long getMinAge() {
        return minAge;
    }

    public void setMinAge(Long minAge) {
        this.minAge = minAge;
    }

    public Long getMaxAge() {
        return maxAge;
    }

    public void setMaxAge(Long maxAge) {
        this.maxAge = maxAge;
    }

    public User getAdmin() {
        return admin;
    }

    public void setAdmin(User admin) {
        this.admin = admin;
    }

    public List<User> getParticipants() {
        return participants;
    }

    public void setParticipants(List<User> participants) {
        this.participants = participants;
    }

    public List<User> getQueue() {
        return queue;
    }

    public void setQueue(List<User> queue) {
        this.queue = queue;
    }

    @Override
    public String toString() {
        return "Event{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", category='" + category + '\'' +
                ", pictureURL='" + pictureURL + '\'' +
                ", address='" + address + '\'' +
                ", timeCreated=" + timeCreated +
                ", timeStarting=" + timeStarting +
                ", deadline=" + deadline +
                ", maxParticipants=" + maxParticipants +
                ", minAge=" + minAge +
                ", maxAge=" + maxAge +
                ", admin=" + admin +
                ", participants=" + participants +
                ", queue=" + queue +
                '}';
    }
}