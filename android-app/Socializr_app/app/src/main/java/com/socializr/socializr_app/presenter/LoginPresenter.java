package com.socializr.socializr_app.presenter;

import android.util.Log;

import com.socializr.socializr_app.networking.marshall.LoginMarshall;
import com.socializr.socializr_app.application.Session;
import com.socializr.socializr_app.view.LoginActivity;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class LoginPresenter extends AbstractPresenter {
    private static final String TAG = "LoginPresenter";
    private LoginActivity activity;

    public LoginPresenter(LoginActivity activity) {
        super();
        this.activity = activity;
    }

    public void login(String email, String password) {
        Log.d("HTTP", String.format("login called w/ email %s and password %s", email, password));

        LoginMarshall loginMarshall = new LoginMarshall(email, password);

        restClient.login(loginMarshall, new Callback<String>() {
            @Override
            public void onResponse(Response<String> response, Retrofit retrofit) {
                Log.d("HTTP", "login status " + response.code());

                if (response.isSuccess()) {
                    Session.getInstance().setToken(response.body());

                    activity.onLoginSuccess();
                } else {
                    Log.d(TAG, "login failed with response " + response.code());

                    activity.onLoginFailed(response.code());
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.d("HTTP", "login failed: " + t.getMessage());

                activity.onLoginFailed(500);
            }
        });
    }
}
