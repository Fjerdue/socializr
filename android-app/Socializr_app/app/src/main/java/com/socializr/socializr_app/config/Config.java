package com.socializr.socializr_app.config;

import android.os.Build;

public final class Config {
    public static final boolean EMULATOR = Build.PRODUCT.contains("sdk") || Build.MODEL.contains("Emulator");
}