package com.socializr.socializr_app.view.EventStack;

import android.media.Image;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import com.daprlabs.aaron.swipedeck.SwipeDeck;
import com.socializr.socializr_app.R;
import com.socializr.socializr_app.view.Adapters.SwipeDeckAdapter;

public class EventTabFragment extends Fragment {
    private SwipeDeck cardStack;
    private SwipeDeckAdapter adapter;
    private ImageButton swipeLeftButton, swipeRightButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_event_tab, container, false);

        cardStack = (SwipeDeck) view.findViewById(R.id.swipe_deck);

        adapter = new SwipeDeckAdapter(getActivity());
        if(cardStack != null){
            cardStack.setAdapter(adapter);
        }

        // TODO CALLBACK
        cardStack.setCallback(new SwipeDeck.SwipeDeckCallback() {
            @Override
            public void cardSwipedLeft(long stableId) {
                Log.i("EventFragment", "card was swiped left, position in adapter: " + stableId);
            }

            @Override
            public void cardSwipedRight(long stableId) {
                Log.i("EventFragment", "card was swiped right, position in adapter: " + stableId);
            }
        });

        cardStack.setLeftImage(R.id.left_image);
        cardStack.setRightImage(R.id.right_image);

        swipeLeftButton = (ImageButton) view.findViewById(R.id.swipe_left_button);
        swipeLeftButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cardStack.swipeTopCardLeft(180);
            }
        });
        swipeRightButton = (ImageButton) view.findViewById(R.id.swipe_right_button);
        swipeRightButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cardStack.swipeTopCardRight(180);
            }
        });

        return view;
    }

}
