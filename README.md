# Socializr

## Installation
- Hent NodeJS fra [https://nodejs.org/en/download/](https://nodejs.org/en/download/)
- Klon repo'et
- Åben folderen repo'et er klonet til, i kommandolinjen og kør *npm install*
- Dette henter alle dependencies til backend-applikationen

## Kørsel
- Åben folderen repo'et er klonet til i kommandolinjen og kør *node server.js* eller *nodejs server.js*

## Bonusinfo
- Applikationen kører på port 3000 og kan tilgås gennem en web browser ved at åbne [http://localhost:3000/](http://localhost:3000/)
